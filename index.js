// This file is used to register all your cloud functions in GCP.
// DO NOT EDIT/DELETE THIS, UNLESS YOU KNOW WHAT YOU ARE DOING.

exports.bitmergepythonhirupython = require("./bitmergepython/hirupython.py").handler;
exports.bitmergepythonHirujs = require("./bitmergepython/Hirujs.js").handler;
exports.bitmergepythonsecondpython = require("./bitmergepython/secondpython.py").handler;
exports.bitmergepythonthridpf = require("./bitmergepython/thridpf.py").handler;